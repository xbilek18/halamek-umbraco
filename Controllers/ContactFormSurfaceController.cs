﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using halamek_umbraco.Models;
using System.Net.Mail;

namespace halamek_umbraco.Controllers
{
    public class ContactFormSurfaceController : SurfaceController
    {
        public ContactFormViewModel model { get; set; }
        // GET: ContactFormSurface
        public ActionResult Index()
        {
            return PartialView("ContactForm", new ContactFormViewModel());
        }
        [HttpPost]
        public ActionResult HandleFormSubmit(ContactFormViewModel model) {
            this.model = model;
            if (ModelState.IsValid)
            {
                sendContactMessage();
                return setRedirectResult(true);
            }
            else {
                return setRedirectResult(false);
            }
        }

        private RedirectResult setRedirectResult(bool result)
        {
            TempData["success"] = result;
            return new RedirectResult($"{CurrentPage.Url}#thanks");
        }

        private void sendContactMessage()
        {
            MailMessage message = getMessageFromModel();
            SmtpClient mailSender = new SmtpClient();
            mailSender.Send(message);
        }

        private MailMessage getMessageFromModel() {
            MailMessage message = new MailMessage();
            message.To.Add("honza@elisiondesign.cz");
            message.From = new MailAddress(model.Email, model.Name);
            message.Body = model.Message;
            message.Subject = "Halamek.co: Contact Form Message";
            return message;
        }
    }
}